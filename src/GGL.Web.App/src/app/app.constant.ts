export const APP_HEADERS = {
	AUTHORIZATION: 'Authorization',
	CONTENT_TYPE: 'Content-Type',
	ACCEPT: 'application/json'
};

export const APP_FOLDERS = {
	GRAPHICS_FOLDER_PATH: 'assets/graphics/',
	IMAGES_FOLDER_PATH: 'assets/graphics/images/',
	IMAGES_GALLERY_FOLDER_PATH: 'assets/graphics/images/gallery/',
	ICONS_FOLDER_PATH: 'assets/graphics/icons/',
	ICONS_SVG_FOLDER_PATH: 'assets/graphics/icons/svg/'
};


