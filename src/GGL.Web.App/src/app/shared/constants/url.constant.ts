export const CLIENT_URL = {
	HOME: 'admin/home',
	DASHBOARD: 'admin/dashboard',
	BOOKINGS: 'admin/bookings',
	BOOKING_DETAIL: 'admin/booking'
};
