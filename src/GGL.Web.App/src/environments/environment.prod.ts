export const environment = {
	production: true,
	clientUrl: 'http://localhost:4200/',
	apiUrl: 'http://localhost:5000/'
};
